# -*- coding: utf-8 -*-

class CardBase(object):
    def __init__(self, db_card=None):
        if not db_card:
            self.name = ''
            self.img = ''
            self.description = ''
        else:
            self.name = db_card.name
            self.img = db_card.img
            self.description = db_card.description

class ThreatRating(object):
    def __init__(self, db_card=None):
        if not db_card:
            self.attack = 0
            self.body = 0
            self.armor = 0
        else:
            self.attack = db_card.description
            self.body = db_card.body
            self.armor = db_card.armor

class DeployCost(object):
    def __init__(self, db_card=None):
        if not db_card:
            self.deploy_cost = 0
            self.unkeep_cost = 0
        else:
            self.deploy_cost = db_card.deploy_cost
            self.unkeep_cost = db_card.unkeep_cost