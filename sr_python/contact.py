# -*- coding: utf-8 -*-
from abstract import *

class Contact(CardBase, DeployCost):
    def __init__(self, db_card=None):
        super(Contact, self).__init__(db_card)
        if not db_card:
            self.race = ''
        else:
            self.race = db_card.race