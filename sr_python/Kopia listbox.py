# -*- coding: utf-8 -*-
import pygame
from pygame.locals import *
from elixir import *
from location import Location
from pool import Pool

class Card(pygame.sprite.Sprite):
    def __init__(self, listbox, pos, *groups):
        super(Card, self).__init__(*groups)
        self.updated = False
        self.listbox = listbox
        self.pos = pos
        self.image = self.listbox.imgs[self.listbox.pos]
        self.rect = pygame.rect.Rect((self.listbox.x+20+330*(self.pos), self.listbox.y), self.image.get_size())

        '''
        if self.pos == 0:
            self.image = self.listbox.imgs_s[self.pos]
            self.rect = pygame.rect.Rect((self.listbox.x, self.listbox.y+15), self.image.get_size())
        elif self.pos > self.listbox.visible_amount:
            self.image = self.listbox.imgs_s[self.pos]
            self.rect = pygame.rect.Rect((self.listbox.x+60+220*(self.pos-2), self.listbox.y+15), self.image.get_size())
        else:
            self.image = self.listbox.imgs[self.pos]
            self.rect = pygame.rect.Rect((self.listbox.x+20+220*(self.pos-1), self.listbox.y), self.image.get_size())
        '''
    def update(self, dt, game):
        if not self.updated:
            self.image = self.listbox.imgs[self.listbox.pos+self.pos]
            self.rect = pygame.rect.Rect((self.listbox.x+20+330*(self.pos), self.listbox.y), self.image.get_size())
            self.updated = True

        


class ListBox(object):
    def __init__(self, list, visible_amount, x, y):
        self.imgs = []
        self.imgs_s = []
        self.imgs_b = []
        self.x = x
        self.y = y
        self.pos = 0
        self.visible_amount = visible_amount
        for item in list:
            img_oryg = pygame.image.load(item.img)#.convert_alpha()
            if  type(item) == Location:
                img_s = pygame.transform.smoothscale(img_oryg, (278,200))
                img = pygame.transform.smoothscale(img_oryg, (320,230))
                img_b = pygame.transform.smoothscale(img_oryg, (480,345))
                #img_s = pygame.transform.smoothscale(img_oryg, (250,180))
            else:
                img_s = pygame.transform.smoothscale(img_oryg, (200,278))
                img = pygame.transform.smoothscale(img_oryg, (230,320))
                img_b = pygame.transform.smoothscale(img_oryg, (345,480))
                #img_s = pygame.transform.smoothscale(img_oryg, (180,250))
            self.imgs.append(img)
            self.imgs_s.append(img_s)
            self.imgs_b.append(img_b)
        self.cards = pygame.sprite.Group()
        #self.card_l = pygame.sprite.Group()
        #self.card_r = pygame.sprite.Group()
        #for item in list:
        #    Card(self, len(self.cards), item, self.cards)
        for i in range(min(self.visible_amount, len(list))):
            Card(self, i, self.cards)
        #Card(self, 0, self.card_l)
        #Card(self, i+2, self.card_r)
    def update(self, dt, game):
            self.cards.update(dt / 1000., self)
            #self.card_r.update(dt / 1000., self)
            #self.card_l.update(dt / 1000., self)
    def draw(self, screen):
            #self.card_l.draw(screen)
            #self.card_r.draw(screen)
            self.cards.draw(screen)
    def go_right(self):
        pass

class Game(object):
    def main(self, screen):
        pool = Pool()
        clock = pygame.time.Clock()

        print 'kart razem: %d' % len(pool.cards)
        self.listbox = ListBox(pool.cards, 3, 50, 200)

        while 1:
            dt = clock.tick(30)

            for event in pygame.event.get():
                if event.type == QUIT:
                    return
                if event.type == KEYDOWN and event.key == K_ESCAPE:
                    return


            self.listbox.update(dt / 1000., self)
            screen.fill((0, 0, 0))
            self.listbox.draw(screen)
            pygame.display.flip()


if __name__ == '__main__':
    setup_all()
    pygame.init()
    screen = pygame.display.set_mode((0, 0), 0*FULLSCREEN)#(1024, 600))
    Game().main(screen)
