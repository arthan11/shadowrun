# -*- coding: utf-8 -*-
from srkg import *
from elixir import *

setup_all()

#create_all()

def select_or_add(table, a_name):
    sel = table.query.filter(table.name==a_name).all()
    if len(sel) > 0:
        return sel[0]
    else:
        return table(name=a_name)
        
def select(table, a_name):
    try:
        return table.query.filter(table.name == a_name).one()
    except:
        print 'brak wymaganych danych: "%s"' % a_name
        import sys
        sys.exit()

'''
# initial data

# special traits
x = SpecialTraits(name = u'antyspołeczny')
x = SpecialTraits(name = 'biotech')
x = SpecialTraits(name = u'sława')
x = SpecialTraits(name = u'strażnik')
x = SpecialTraits(name = 'odludek')
x = SpecialTraits(name = 'zwiadowca')
x = SpecialTraits(name = u'wytrzymałość')
# races
x = Race(name = u'człowiek')
x = Race(name = 'elf')
x = Race(name = 'krasnolud')
x = Race(name = 'ork')
x = Race(name = 'troll')

# skills
x = Skill(name = 'atletyka')
x = Skill(name = u'przywoływanie')
x = Skill(name = 'dekowanie')
x = Skill(name = 'saper')
x = Skill(name = u'broń palna')
x = Skill(name = u'broń ciężka')
x = Skill(name = u'przywództwo')
x = Skill(name = u'walka wręcz')
x = Skill(name = u'pilotaż')
x = Skill(name = u'społeczne')
x = Skill(name = 'magia')
x = Skill(name = 'maskowanie')
x = Skill(name = 'ulica')
x = Skill(name = 'technika')

# base gear types
x = GearType(name = u'akcesoria')
x = GearType(name = u'pancerz')
x = GearType(name = u'cyberwszczep')
x = GearType(name = u'robot')
x = GearType(name = u'duch')
x = GearType(name = u'magiczny')
x = GearType(name = u'matryca')
x = GearType(name = u'różne')
x = GearType(name = u'pojazd')
x = GearType(name = u'broń')
# other gear types
x = GearType(name = u'czar', base_type=select(GearType, u'magiczny'))
x = GearType(name = u'broń strzelecka', base_type=select(GearType, u'broń'))
x = GearType(name = u'karabin szturmowy', base_type=select(GearType, u'broń strzelecka'))
x = GearType(name = u'cyberdek', base_type=select(GearType, u'matryca'))

# objectives
x = Objective(name='ATAK KAMIKADZE', img='Atak Kamikadze.jpg', reputation_points=20, description='')
x = Objective(name=u'CHRONIĆ I BRONIĆ', img=u'Chronic i Bronic.jpg', reputation_points=20, description='')
x = Objective(name=u'CZARNA KSIĘGA DUNKELZAHNA', img=u'Czarna Ksiega Dunkelzahna.jpg', reputation_points=40, description='')
x = Objective(name=u'CZYSTKA W GNIEŹDZIE', img=u'Czystka w Gniezdzie.jpg', reputation_points=40, description='')
x = Objective(name=u'EKO-WOJNA!', img=u'Eko-Wojna!.jpg', reputation_points=25, description='')
x = Objective(name=u'FORT KNOCKS', img=u'Fort Knocks.jpg', reputation_points=35, description='')

# runners        
r = Runner(
    name='ADAM BOMBA',
    img='Adam Bomba.jpg',
    description='',
    attack=2,
    body=3,
    armor=0,
    deploy_cost=3,
    unkeep_cost=0,
    race=select(Race, 'krasnolud'),
    proffession=select_or_add(Proffession, u'SŁYNNY ROCKER'),
    prime_runner=False
    )
rst = RunnersSpecialTraits(runner=r, special_trait=select(SpecialTraits, u'sława'), level=2)

r = Runner(
    name='ARCHIE MCDEVEN',
    img='Archie McDeven.jpg',
    description='',
    attack=4,
    body=3,
    armor=0,
    deploy_cost=4,
    unkeep_cost=0,
    race=select(Race, u'człowiek'),
    proffession=select_or_add(Proffession, u'DETEKTYW'),
    prime_runner=False
    )
rs = RunnersSkills(runner=r, skill=select(Skill, u'broń palna'), level=1)
rs = RunnersSkills(runner=r, skill=select(Skill, u'społeczne'), level=1)

r = Runner(
    name='BAM-BAM',
    img='Bam-Bam.jpg',
    description='',
    attack=6,
    body=8,
    armor=1,
    deploy_cost=8,
    unkeep_cost=0,
    race=select(Race, u'troll'),
    proffession=select_or_add(Proffession, u'ULICZNY SAMURAJ'),
    prime_runner=False
    )
rs = RunnersSkills(runner=r, skill=select(Skill, u'broń ciężka'), level=1)
rs = RunnersSkills(runner=r, skill=select(Skill, u'walka wręcz'), level=2)

r = Runner(
    name='BLASZAK',
    img='Blaszak.jpg',
    description='',
    attack=5,
    body=5,
    armor=1,
    deploy_cost=8,
    unkeep_cost=0,
    race=select(Race, u'krasnolud'),
    proffession=select_or_add(Proffession, u'CYBORG'),
    prime_runner=False
    )
rst = RunnersSpecialTraits(runner=r, special_trait=select(SpecialTraits, u'wytrzymałość'), level=1)
rs = RunnersSkills(runner=r, skill=select(Skill, u'technika'), level=1)

r = Runner(
    name='BRZYTWA',
    img='Brzytwa.jpg',
    description='',
    attack=6,
    body=6,
    armor=0,
    deploy_cost=8,
    unkeep_cost=0,
    race=select(Race, u'ork'),
    proffession=select_or_add(Proffession, u'ULICZNY SAMURAJ'),
    prime_runner=False
    )
rs = RunnersSkills(runner=r, skill=select(Skill, u'atletyka'), level=1)
rs = RunnersSkills(runner=r, skill=select(Skill, u'broń palna'), level=1)
rs = RunnersSkills(runner=r, skill=select(Skill, u'walka wręcz'), level=2)


# challenges
c = Challenge(
    name=u'DIABELNY KOREK',
    img=u'diabelny korek.png',
    description=u'',
    attack=0,
    body=0,
    armor=0
    )
cs = ChallengesSkills(challenge=c, skill=select(Skill, u'pilotaż'), level=2, group=1)
cs = ChallengesSkills(challenge=c, skill=select(Skill, u'ulica'), level=2, group=2)
c.keywords.append(select_or_add(ChallengesKeywords, u'ulica'))
c.keywords.append(select_or_add(ChallengesKeywords, u'pojazd'))
c.keywords.append(select_or_add(ChallengesKeywords, u'otwarta przestrzeń'))

c = Challenge(
    name=u'ELEKTRYCZNE OGRODZENIE',
    img=u'eleketryczne ogrodzenie.png',
    description=u'',
    attack=0,
    body=10,
    armor=1
    )
cs = ChallengesSkills(challenge=c, skill=select(Skill, u'ulica'), level=1, group=1)
cs = ChallengesSkills(challenge=c, skill=select(Skill, u'technika'), level=1, group=2)
c.keywords.append(select_or_add(ChallengesKeywords, u'elektryczne'))
c.keywords.append(select_or_add(ChallengesKeywords, u'zapora'))

c = Challenge(
    name=u'ELITARNI STRAŻNICY',
    img=u'elitarni straznicy.png',
    description=u'',
    attack=5,
    body=7,
    armor=1
    )
cs = ChallengesSkills(challenge=c, skill=select(Skill, u'maskowanie'), level=1, group=1)
cs = ChallengesSkills(challenge=c, skill=select(Skill, u'społeczne'), level=2, group=1)
c.keywords.append(select_or_add(ChallengesKeywords, u'pomieszczenie'))
c.keywords.append(select_or_add(ChallengesKeywords, u'personel'))

c = Challenge(
    name=u'JEDZĄCE GHULE',
    img=u'jedzace ghule.png',
    description=u'',
    attack=5,
    body=10,
    armor=0
    )
cs = ChallengesSkills(challenge=c, skill=select(Skill, u'maskowanie'), level=2, group=1)
cs = ChallengesSkills(challenge=c, skill=select(Skill, u'ulica'), level=1, group=1)
c.keywords.append(select_or_add(ChallengesKeywords, u'pomieszczenie'))
c.keywords.append(select_or_add(ChallengesKeywords, u'przebudzony'))

c = Challenge(
    name=u'OCZOBÓJCA',
    img=u'oczobojca.png',
    description=u'',
    attack=5,
    body=4,
    armor=0
    )
c.keywords.append(select_or_add(ChallengesKeywords, u'otwarta przestrzeń'))
c.keywords.append(select_or_add(ChallengesKeywords, u'przebudzony'))

c = Challenge(
    name=u'OPŁACENI RUNNERZY',
    img=u'oplaceni runnerzy.png',
    description=u'',
    attack=8,
    body=8,
    armor=1
    )
cs = ChallengesSkills(challenge=c, skill=select(Skill, u'społeczne'), level=2, group=1)
cs = ChallengesSkills(challenge=c, skill=select(Skill, u'ulica'), level=2, group=2)
c.keywords.append(select_or_add(ChallengesKeywords, u'ulica'))

c = Challenge(
    name=u'RÓJ ROBOTÓW',
    img=u'roj robotow.png',
    description=u'',
    attack=3,
    body=12,
    armor=1
    )
cs = ChallengesSkills(challenge=c, skill=select(Skill, u'atletyka'), level=2, group=1)
c.keywords.append(select_or_add(ChallengesKeywords, u'otwarta przestrzeń'))
c.keywords.append(select_or_add(ChallengesKeywords, u'elektryczne'))

c = Challenge(
    name=u'WAL I ZWIEWAJ',
    img=u'wal i zwiewaj.png',
    description=u'',
    attack=0,
    body=0,
    armor=0
    )
cs = ChallengesSkills(challenge=c, skill=select(Skill, u'atletyka'), level=2, group=1)
cs = ChallengesSkills(challenge=c, skill=select(Skill, u'ulica'), level=1, group=1)
c.keywords.append(select_or_add(ChallengesKeywords, u'otwarta przestrzeń'))
c.keywords.append(select_or_add(ChallengesKeywords, u'ulica'))

# contacts
c = Contact(
    name=u'LEWY CHIRURG',
    img=u'lewy chirurg.png',
    description=u'',
    deploy_cost=2,
    unkeep_cost=0,
    race=select(Race, u'człowiek'),
    )

c = Contact(
    name=u'PAN JOHNSON',
    img=u'pan johnson.png',
    description=u'',
    deploy_cost=3,
    unkeep_cost=0,
    race=select(Race, u'człowiek'),
    )

c = Contact(
    name=u'PANIUSIA Z KOSZYKIEM',
    img=u'paniusia z koszykiem.png',
    description=u'',
    deploy_cost=2,
    unkeep_cost=0,
    race=select(Race, u'człowiek'),
    )

# gears
g = Gear(
    name=u'AUTOMAT POJAZD PATROLOWY',
    img=u'automat pojazd patrolowy.png',
    description=u'',
    deploy_cost=6,
    unkeep_cost=0,
    gear_type=select(GearType, u'robot'),
    )

g = Gear(
    name=u'BULLDOG VAN',
    img=u'bulldog van.png',
    description=u'',
    deploy_cost=4,
    unkeep_cost=0,
    gear_type=select(GearType, u'pojazd'),
    )

g = Gear(
    name=u'CIĘŻKI PANCERZ (PEŁNY)',
    img=u'ciezki pancerz (pelny).png',
    description=u'',
    deploy_cost=6,
    unkeep_cost=0,
    gear_type=select(GearType, u'pancerz'),
    )

g = Gear(
    name=u'CYBERRAMIĘ',
    img=u'cyberramie.png',
    description=u'',
    deploy_cost=2,
    unkeep_cost=0,
    gear_type=select(GearType, u'cyberwszczep'),
    )

g = Gear(
    name=u'CZAR PANCERZ',
    img=u'czar pancerz.png',
    description=u'',
    deploy_cost=4,
    unkeep_cost=0,
    gear_type=select(GearType, u'czar'),
    )

g = Gear(
    name=u'DUCH OBSERWATOR',
    img=u'duch obserwator.png',
    description=u'',
    deploy_cost=3,
    unkeep_cost=0,
    gear_type=select(GearType, u'duch'),
    )

g = Gear(
    name=u'FN HAR.',
    img=u'fn har.png',
    description=u'',
    deploy_cost=4,
    unkeep_cost=0,
    gear_type=select(GearType, u'broń strzelecka'),
    )

g = Gear(
    name=u'FUCHI CYBER-6',
    img=u'fuchi cyber-6.png',
    description=u'',
    deploy_cost=3,
    unkeep_cost=0,
    gear_type=select(GearType, u'cyberdek'),
    )

# locations
l = Location(
    name=u'AZTECHNOLOGIA',
    img=u'Aztechnologia.jpg',
    description=u'',
    deploy_cost=3,
    unkeep_cost=0,
    )

l = Location(
    name=u'DZIURAWY KIEŁ',
    img=u'Dziurawy Kiel.jpg',
    description=u'',
    deploy_cost=3,
    unkeep_cost=0,
    )

l = Location(
    name=u'FUCHI INDUSTRIES',
    img=u'Fuchi Industries.jpg',
    description=u'',
    deploy_cost=4,
    unkeep_cost=0,
    )

l = Location(
    name=u'GNIAZDO SNAJPERSKIE',
    img=u'Gniazdo Snajperskie.jpg',
    description=u'',
    deploy_cost=4,
    unkeep_cost=0,
    )

l = Location(
    name=u'JASKINIE HALVERVILLE',
    img=u'Jaskinie Halverville.jpg',
    description=u'',
    deploy_cost=3,
    unkeep_cost=0,
    )

# specials
s = Special(
    name=u'BEZ WYJŚCIA',
    img=u'Bez wyjscia.jpg',
    description=u'',
    deploy_cost=1,
    unkeep_cost=0,
    stinger=True
    )

s = Special(
    name=u'BIJATYKA W BARZE',
    img=u'Bijatyka w barze.jpg',
    description=u'',
    deploy_cost=0,
    unkeep_cost=0,
    stinger=False
    )

s = Special(
    name=u'CYBER-PSYCHOZA',
    img=u'Cyber-Psychoza.jpg',
    description=u'',
    deploy_cost=2,
    unkeep_cost=0,
    stinger=False
    )

s = Special(
    name=u'CZERWONY ALARM',
    img=u'Czerwony Alarm.jpg',
    description=u'',
    deploy_cost=6,
    unkeep_cost=0,
    stinger=False
    )

s = Special(
    name=u'DEJA VU',
    img=u'Deja vu.jpg',
    description=u'',
    deploy_cost=0,
    unkeep_cost=0,
    stinger=True
    )

s = Special(
    name=u'FAŁSZYWY ADRES',
    img=u'Falszywy adres.jpg',
    description=u'',
    deploy_cost=4,
    unkeep_cost=0,
    stinger=True
    )

s = Special(
    name=u'FAŁSZYWY MENTOR',
    img=u'Falszywy mentor.jpg',
    description=u'',
    deploy_cost=6,
    unkeep_cost=0,
    stinger=True
    )

s = Special(
    name=u'IMPREZKA',
    img=u'Imprezka.jpg',
    description=u'',
    deploy_cost=0,
    unkeep_cost=0,
    stinger=False
    )

s = Special(
    name=u'KANTOWANE KOŚCI',
    img=u'Kantowane kosci.jpg',
    description=u'',
    deploy_cost=0,
    unkeep_cost=0,
    stinger=True
    )

s = Special(
    name=u'MARSZ ZIELONEGO JABŁUSZKA',
    img=u'Marsz zielonego jabluszka.jpg',
    description=u'',
    deploy_cost=2,
    unkeep_cost=0,
    stinger=True
    )


'''

#session.commit()

def CardsCount():
    count = 0
    x = Objective.query.all()
    count += len(x)
    x = Runner.query.all()
    count += len(x)
    x = Challenge.query.all()
    count += len(x)
    x = Contact.query.all()
    count += len(x)
    x = Location.query.all()
    count += len(x)
    x = Gear.query.all()
    count += len(x)
    x = Special.query.all()
    count += len(x)
    return count


print CardsCount()

    


#import sqlalchemy as sa
#sa.update(Product.table).where(Product.id > 100).values(price=None)

#x = Objective.query.filter(Objective.id == 1).one()
#obj = x[0]
#obj.img = u'Atak Kamikadze.jpg'







'''
# print runners
xs = Runner.query.all()
print '%d \n' % len(xs)
for x in xs:
    print '%d %s * %s' % (x.id, x.name, x.proffession.name)
    print '  %d/%dY' % (x.deploy_cost, x.unkeep_cost)
    print '  %d/%d(P%d)' % (x.attack, x.body, x.armor)
    print '  %s %s' % (x.race.name, x.img)
    
    rs = RunnersSkills.query.filter(RunnersSkills.runner.has(id=x.id)).all()
    print '  skills: %d' % len(rs)
    for a in rs:
        print '  %s %d' % (a.skill.name, a.level)
    print ''

    rst = RunnersSpecialTraits.query.filter(RunnersSpecialTraits.runner.has(id=x.id)).all()
    print '  Special Traits: %d' % len(rst)
    for a in rst:
        print '  %s %d' % (a.special_trait.name, a.level)
    print ''
    

# print challenges    
xs = Challenge.query.all()
print '%d \n' % len(xs)
for x in xs:
    print '%d %s' % (x.id, x.name)
    print '  %d/%d(P%d)' % (x.attack, x.body, x.armor)
    print '  %s' % (x.img)

    keyws = []
    for k in x.keywords:
        keyws.append(k.name)
    print u'  słowa kluczowe: %s \n' % ', '.join(keyws)

    
    keyws = []
    for i, k in enumerate(x.skills):
        keyws.append('%s(%d)' % (k.skill.name, k.level))
        if i > 0:
            if x.skills[i-1].group <> x.skills[i].group:
                keyws[len(keyws)-1] = ' lub ' + keyws[len(keyws)-1]
    print u'  warunki wykręcenia: %s \n' % ', '.join(keyws)
    print ''

'''
