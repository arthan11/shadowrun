import unittest
from dice import Dice

class CheckDice(unittest.TestCase):
    def setUp(self):
        self.dice = Dice()

    def test_default(self):
        for i in range(1, 100):
            x = self.dice.roll()
            self.assertIn( x, range(1, 7))

    def test_8_sides(self):
        for i in range(1, 100):
            self.dice.sides = 8
            x = self.dice.roll()
            self.assertIn( x, range(1, 9))

    def test_6_sides_plus_3(self):
        for i in range(1, 100):
            x = self.dice.roll(+3)
            self.assertIn( x, range(4, 10))

    def test_6_sides_minus_3(self):
        for i in range(1, 100):
            x = self.dice.roll(-3)
            self.assertIn( x, range(1, 4))
