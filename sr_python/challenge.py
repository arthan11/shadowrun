# -*- coding: utf-8 -*-
from abstract import *
from sr_types import Races
from skill import Skill

class Challenge(CardBase, ThreatRating):
    def __init__(self, db_card=None):
        super(Challenge, self).__init__(db_card)
        self.keywords = []
        self.skills = []
        if db_card:
            for db_k in db_card.keywords:
                self.keywords.append(db_k.name)
            for db_s in db_card.skills:
                s = Skill(type=db_s.skill.id, name=db_s.skill.name, level=db_s.level)
                self.skills.append(s)