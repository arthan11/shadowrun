import random

class Dice(object):
    def __init__(self):
        self.sides = 6
    def roll(self, modyfier = 0):
        result = random.randint(1, self.sides) + modyfier;
        if result < 1:
          result = 1
        return result


if __name__ == "__main__":
    dice = Dice()
    for i in range(10):
        print dice.roll()
