# -*- coding: utf-8 -*-
from elixir import *

#metadata.bind = "sqlite:///srkg.db?charset=utf8"
metadata.bind = "sqlite:///srkg.db"
metadata.bind.echo = False#True


# abstract classes
class ThreatRating(Entity):
    using_options(inheritance='multi', abstract='True')
    attack = Field(Integer, default=0, nullable=False)
    body = Field(Integer, default=0, nullable=False)
    armor = Field(Integer, default=0, nullable=False)

class DeployCost(Entity):
    using_options(inheritance='multi', abstract='True')
    deploy_cost = Field(Integer, default=0, nullable=False)
    unkeep_cost = Field(Integer, default=0, nullable=False)

class Named(Entity):
    using_options(inheritance='multi', abstract='True')
    name = Field(Unicode(100))
    def __repr__(self):
        return '<%s "%s">' % (self.__class__.__name__, self.name.encode('utf-8'))

class NamedWithImg(Named):
    using_options(inheritance='multi', abstract='True')
    img = Field(Unicode(200))

class CardBase(NamedWithImg):
    using_options(inheritance='multi', abstract='True')
    description = Field(Unicode(200))

# some other classes than cards
class SpecialTraits(Named):
    pass

class Race(Named):
    runner = ('Runner')
    contact = OneToMany('Contact')

class Proffession(Named):
    runner = OneToMany('Runner')

class GearType(Named):
    base_type = ManyToOne('GearType')
    subtypes = OneToMany('GearType')
    gear = OneToMany('Gear')

class Skill(NamedWithImg):
    pass

class RunnersSkills(Entity):
    runner = ManyToOne('Runner')
    skill = ManyToOne('Skill')
    level = Field(Integer, default=1, nullable=False)    
    def __repr__(self):
        return "<RunnersSkills %s - %s %d>" % (self.runner.name, self.skill.name, self.level) 

class RunnersSpecialTraits(Entity):
    runner = ManyToOne('Runner')
    special_trait = ManyToOne('SpecialTraits')
    level = Field(Integer, default=1, nullable=False)

class ChallengesSkills(Entity):
    challenge = ManyToOne('Challenge')
    skill = ManyToOne('Skill')
    level = Field(Integer, default=1, nullable=False)
    group = Field(Integer, default=1, nullable=False)

class ChallengesKeywords(Named):
    challenges = ManyToMany('Challenge')

# seven types of cards    
class Runner(CardBase, ThreatRating, DeployCost):
    proffession = ManyToOne('Proffession')
    race = ManyToOne('Race')
    prime_runner = Field(Boolean, default=False, nullable=False)
    skills = OneToMany('RunnersSkills')    
    SpecialTraits = OneToMany('RunnersSpecialTraits')

class Challenge(CardBase, ThreatRating):
    keywords = ManyToMany(ChallengesKeywords)
    skills = OneToMany('ChallengesSkills')

class Gear(CardBase, DeployCost):
    gear_type = ManyToOne('GearType')

class Location(CardBase, DeployCost):
    pass

class Contact(CardBase, DeployCost):
    race = ManyToOne('Race')

class Objective(CardBase):
    reputation_points = Field(Integer, default=0, nullable=False)

class Special(CardBase, DeployCost):
    stinger = Field(Boolean, default=False, nullable=False)

if __name__ == "__main__":
    setup_all()
    create_all()