# -*- coding: utf-8 -*-
from elixir import *
import srkg as DB

from runner import Runner
from challenge import Challenge
from contact import Contact
from location import Location
from objective import Objective
from special import Special
from gear import Gear

from skill import Skill
from special_trait import SpecialTrait

class Pool(object):
    def __init__(self):
        self.cards = []
        self.load_data()

    def load_data(self):
        self.load_challenges()
        self.load_locations()
        self.load_runners()

        self.load_contacts()

        self.load_objectives()
        self.load_specials()
        self.load_gears()

    def load_runners(self):
        rows = DB.Runner.query.all()
        for db_card in rows:
            card = Runner(db_card)
            card.img = '..\\graphics\\SR karty\\Runnerzy\\' + card.img
            self.cards.append(card)

    def load_challenges(self):
        rows = DB.Challenge.query.all()
        for db_card in rows:
            card = Challenge(db_card)
            card.img = '..\\graphics\\SR karty\\Przeszkody moje\\' + card.img
            self.cards.append(card)

    def load_contacts(self):
        rows = DB.Contact.query.all()
        for db_card in rows:
            card = Contact(db_card)
            card.img = '..\\graphics\\SR karty\\Kontakty moje\\' + card.img
            self.cards.append(card)

    def load_locations(self):
        rows = DB.Location.query.all()
        for db_card in rows:
            card = Location(db_card)
            card.img = '..\\graphics\\SR karty\\Miejsca\\' + card.img
            self.cards.append(card)

    def load_objectives(self):
        rows = DB.Objective.query.all()
        for db_card in rows:
            card = Objective()
            card.name = db_card.name
            card.img = '..\\graphics\\SR karty\\Cele\\' + db_card.img
            card.description = db_card.description
            card.reputation_points = db_card.reputation_points
            self.cards.append(card)

    def load_specials(self):
        rows = DB.Special.query.all()
        for db_card in rows:
            card = Special()
            card.name = db_card.name
            card.img = '..\\graphics\\SR karty\\Specjalne\\' + db_card.img
            card.description = db_card.description
            card.stinger = db_card.stinger
            self.cards.append(card)

    def load_gears(self):
        rows = DB.Gear.query.all()
        for db_card in rows:
            card = Gear()
            card.name = db_card.name
            card.img = '..\\graphics\\SR karty\\Sprzet moje\\' + db_card.img
            card.description = db_card.description
            card.deploy_cost = db_card.deploy_cost
            card.unkeep_cost = db_card.unkeep_cost
            card.gear_type = db_card.gear_type
            self.cards.append(card)

if __name__ == "__main__":
    setup_all()

    pool = Pool()
    print 'razem unikalnych kart: %d' % len(pool.cards)