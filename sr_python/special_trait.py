# -*- coding: utf-8 -*-

class SpecialTrait(object):
    def __init__(self, type, name, level=1):
        self.name = name
        self.type = type
        self.level = level