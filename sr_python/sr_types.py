# -*- coding: utf-8 -*-

def enum(**enums):
    return type('Enum', (), enums)

Races = enum(Dwarf=3, Human=1, Troll=5, Ork=4, Elf=2)

SkillTypes = enum(Athletics=1, Conjure=2, Decking=3, Demolitions=4, Firearms=5, Gunnery=6,
    Leadership=7, Melee=8, Piloting=9, Social=10, Sorcery=11, Stealth=12, Steetwise=13, Technical=14)

SpecialTraits = enum(stAntiSocial=1, stBiotech=2, stFame=3, stGuard=4, stHermit=5, stRecon=6, stStamina=7)