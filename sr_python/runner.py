# -*- coding: utf-8 -*-
from abstract import *
from sr_types import Races
from skill import Skill
from special_trait import SpecialTrait

class Runner(CardBase, ThreatRating, DeployCost):
    def __init__(self, db_card=None):
        super(Runner, self).__init__(db_card)
        self.skills = []
        self.special_traits = []
        if not db_card:
            self.proffession = ''
            self.race = 0
            self.prime_runner = False
        else:
            self.proffession = db_card.proffession
            self.race = db_card.race
            self.prime_runner = db_card.prime_runner
            for db_s in db_card.skills:
                s = Skill(type=db_s.skill.id, name=db_s.skill.name, level=db_s.level)
                self.skills.append(s)
            for db_s in db_card.SpecialTraits:
                s = SpecialTrait(type=db_s.special_trait.id, name=db_s.special_trait.name, level=db_s.level)
                self.special_traits.append(s)

if __name__ == "__main__":
    r = Runner()
    r.race = Races.Human
    print r.name, r.race