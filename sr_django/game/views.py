#-*- coding: utf-8 -*-
import json
from django.http import HttpResponse
from srkg.models import Objective, Challenge, Contact, Runner, Special, Gear, Location
from random import randint
from itertools import chain
from operator import attrgetter

def jsonDeck(request):
    objectives = Objective.objects.all().order_by('reputation_points')
    objs = []
    for o in objectives:
        obj = {'img': o.img.name, 'id': o.id, 'rep': o.reputation_points, 'type': o.__class__.__name__}
        objs.append(obj)
        # wstawianie w losowej kolejnosci
        #pos = randint(0, len(objs))
        #objs.insert(pos, obj)
    response_data = {}
    response_data['deck'] = objs
    return HttpResponse(json.dumps(response_data), content_type="application/json")

def jsonDeckAll(request):
    #dbcards = list(chain(Challenge.objects.all(), Contact.objects.all()))
    dbcards = sorted(
        chain(Challenge.objects.all(),
              Contact.objects.all(),
              Runner.objects.all(),
              Special.objects.all(),
              Gear.objects.all(),
              Location.objects.all(),
              Objective.objects.all()),
        key=attrgetter('name'))

    cards = []
    for c in dbcards:
        card =  {'img': c.img.name, 'id': c.id, 'type': c.__class__.__name__[:2]}
        cards.append(card)
        # wstawianie w losowej kolejnosci
        #pos = randint(0, len(cards))
        #cards.insert(pos, card)
    response_data = {}
    response_data['deck'] = cards
    return HttpResponse(json.dumps(response_data), content_type="application/json")

def CreateDeck(request):
    response_data = {'status': 'OK'};
    print 'x'
    #print request.body["deck"];
    return HttpResponse(json.dumps(response_data), content_type="application/json")