from django.conf.urls import patterns, url

from game import views

urlpatterns = patterns('',
    url(r'^deck.json$', views.jsonDeck, name='deck'),
    url(r'^deckall.json$', views.jsonDeckAll, name='deckall'),
    url(r'^create_deck$', views.CreateDeck, name='create_deck'),

)