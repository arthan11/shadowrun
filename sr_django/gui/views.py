#-*- coding: utf-8 -*-
from django.shortcuts import render
from django.db.models import Q
from srkg.models import *
from django.db import transaction
import json, time, datetime

def index(request):
    return render(request, 'index.html', {'test': ''})

def shop(request):
    deck = request.GET.get('deck', '{}')
    deck = json.loads(deck)
    Deck.objects.all().delete()
    user = User.objects.get(name='Krzysiek')
    for card in deck:
        for i in range(deck[card]):
            table = card[:2]
            card_id = card[2:]
            d = Deck(user=user, table=table, card_id=card_id)
            d.save()
            print table, card_id
    return render(request, 'shop.html')

def card_list(request):
    gear_types = []
    challenges_keywords = []
    cards = []
    type = request.GET.get('type', '')
    subtype = request.GET.get('subtype', '')
    if (type == 'ch'):
        cards = Challenge.objects.filter(keywords__name=subtype).order_by('name')
        challenges_keywords = ChallengesKeywords.objects.all().order_by('name')
    elif (type == 'ob'):
        cards = Objective.objects.all().order_by('name')
    elif (type == 'co'):
        cards = Contact.objects.all().order_by('name')
    elif (type == 'lo'):
        cards = Location.objects.all().order_by('name')
    elif (type == 'ru'):
        if subtype == 'subtype_ru1':
            cards = Runner.objects.filter(proffession__name='CYBORG').order_by('name')
        elif subtype == 'subtype_ru2':
            cards = Runner.objects.filter(Q(proffession__name='CZŁ. GANGU') | Q(proffession__name='SZEF GANGU')).order_by('name')
        elif subtype == 'subtype_ru3':
            cards = Runner.objects.filter(Q(proffession__name='DEKER') | Q(proffession__name='CUD DZIECKO DEKER')).order_by('name')
        elif subtype == 'subtype_ru4':
            cards = Runner.objects.filter(proffession__name='DETEKTYW').order_by('name')
        elif subtype == 'subtype_ru5':
            cards = Runner.objects.filter(Q(proffession__name='MAG') | Q(proffession__name='MAG WALKI') | Q(proffession__name='ULICZNY MAG') | Q(proffession__name='WYPALONY MAG')).order_by('name')
        elif subtype == 'subtype_ru6':
            cards = Runner.objects.filter(proffession__name='NAJEMNIK').order_by('name')
        elif subtype == 'subtype_ru7':
            cards = Runner.objects.filter(proffession__name='RIGGER').order_by('name')
        elif subtype == 'subtype_ru8':
            cards = Runner.objects.filter(proffession__name='ROCKER').order_by('name')
        elif subtype == 'subtype_ru9':
            cards = Runner.objects.filter(Q(proffession__name='SAMURAJ') | Q(proffession__name='ULICZNY SAMURAJ')).order_by('name')
        elif subtype == 'subtype_ru10':
            cards = Runner.objects.filter(Q(proffession__name='SZAMAN') | Q(proffession__name='SZAMAN SZCZURA') | Q(proffession__name='ULICZNY SZAMAN')).order_by('name')
        elif subtype == 'subtype_ru11':
            cards = Runner.objects.filter(Q(proffession__name='OCHRONIARZ') | Q(proffession__name='SŁYNNA FLIRCIARA')).order_by('name')
    elif (type == 'sp'):
        if (subtype == 'stinger'):
            cards = Special.objects.filter(stinger=True).order_by('name')
        else:
            cards = Special.objects.filter(stinger=False).order_by('name')
    elif (type == 'ge'):
        cards = Gear.objects.filter(gear_type__name=subtype).order_by('name')
        gear_types = GearType.objects.all().order_by('name')
    return render(request, 'card_list.html', {'cards': cards, 'type': type, 'subtype': subtype,
      'gear_types': gear_types, 'challenges_keywords': challenges_keywords})

def ChooseCards(request):
    return render(request, 'choose_cards.html')

def client_test(request):
    return render(request, 'client_test.html')

def client(request):
    users = User.objects.all()
    return render(request, 'client.html', {'users': users})
