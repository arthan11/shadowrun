from django.conf.urls import patterns, url

from gui import views

urlpatterns = patterns('',
    #url(r'^intraday$', views.intraday, name='intraday'),
    url(r'^$', views.index, name='index'),
    url(r'^choose_cards$', views.ChooseCards),
    url(r'^shop$', views.shop),
    url(r'^card_list$', views.card_list),
    url(r'^client_test$', views.client_test),
    url(r'^client$', views.client),
)