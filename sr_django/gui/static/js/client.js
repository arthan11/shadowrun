var
  connected = false,
  gamer_id = -1,
  cele = [],
  nuyens = 0,
  curr_phase = -1,
  curr_phase_name = '',
  curr_gamer = -1,
  iCounter = 0;

function setConnected(IsConnected) {
  connected = IsConnected;
  if (!IsConnected) {
    $('#conn_img').attr('src', '/static/img/gui/disconnect.png');
    $( "#btnLogin" ).button( "option", "disabled", true );
    //ShowMessage("Rozłączono z serwerem.");
  }
  else {
    $('#conn_img').attr('src', '/static/img/gui/connect.png');
    $( "#btnLogin" ).button( "option", "disabled", false );
  };
};

function ShowMessage(msg, title="Uwaga") {
    $( "#dlgMessage" ).html('<p>'+msg+'</p>').attr('title', title).dialog({
        modal: true,
        width: 400,
        resizable: false,
        dialogClass: "",
        closeOnEscape: true,
        buttons: {
            Ok: function() {
                $( this ).dialog( "close" );
            }
        }
    });
};

function ShowAskMessage(msg, title="Uwaga", lbl1="Tak", lbl2="Nie", OnBtn1=null, OnBtn2=null, noClose=false) {
    dlgClass = "";
    if (noClose) {
      dlgClass = "no-close no-title";
    };

    btns = [
      {text: lbl1, click: function() {
        if (OnBtn1 != null) {
          OnBtn1();
        };
        $( this ).dialog( "close" );
      }}
    ]
    if (lbl2 != '') {
      btns.push(
        {text: lbl2, click: function() {
          if (OnBtn2 != null) {
            OnBtn2();
          };
          $( this ).dialog( "close" );
        }}
      );
    };


    $( "#dlgMessage" ).html('<p>'+msg+'</p>').attr('title', title).dialog({
        modal: true,
        width: 400,
        resizable: false,
        dialogClass: dlgClass,
        closeOnEscape: !noClose,
        buttons: btns
    });
};

function DoLogout() {
  socket.doSend({'cmd': 'logout'});
  SetLogged(false);
  $("#userName").text("niezalogowany");
};

function SetLogged(logged) {
  if (logged) {
    $('#btnLogin').off('click').click(function( event ) {
        ShowAskMessage('Czy napewno chcesz się wylogować?', 'Wylogować', 'Tak', 'Nie', function() {DoLogout()});
      });
    $("#btnLogin").button( "option", "label", "Wyloguj" );
  }
  else {
    $('#btnLogin').off('click').click(function( event ) {
        $('#dlgLogin').dialog({
        resizable: false,
        height:150,
        modal: true,
        });
      });
    $("#btnLogin").button( "option", "label", "Zaloguj" );
    gamer_id = -1;
  };
};

function AddNuyens(count) {
  nuyens = nuyens + count;
  $("#nuyens").html(nuyens + " &yen;");
};

function OnTrashHandCard(idx) {
  Hand[idx].move(170, 185);
  Hand[idx].object().off("click");
  Trash.push(Hand[idx]);
  Hand.splice(idx, 1);
  for(i = idx; i < Hand.length; i++) {
    Hand[i].object().attr("idx", i);
  };
  if (Hand.length <= 7) {
    $( "#btnNextPhase" ).button( "option", "disabled", false );
  };
};

function OnDrawObjective() {
  socket.doSend({'cmd': 'draw_objective'});
  if (objective == null) {
    objective = objectivePile.pop();
    objective.object().off('click');
    objective.move(750, 0);
    $( "#btnNextPhase" ).button( "option", "disabled", false );
  };
};

function OnTrashToDrawPile() {
  while(Trash.length > 0) {
    card = Trash.pop();
    card.flip();
    card.move(170, 50);
    drawPile.push(card);
  }
};

function OnLogin(id) {
  gamer_id = id;
  $( "#dlgLogin" ).dialog("close");
  $("#userName").text('Witaj, '+ userName);
  SetLogged(true);

  $('#dlgMainMenu').dialog({
    dialogClass: "no-close no-title",
    closeOnEscape: false,
    resizable: false,
    height:150
  });
};

function DoFaceUpObjective() {
  socket.doSend({'cmd': 'faceup_objective'});
};

function DoTrashCard(event) {
  idx = $(this).attr('idx');
  socket.doSend({'cmd': 'trash_card', 'params': [idx]});
};

function OnFaceUpObjective(player, card) {
  objective.flip('/static/'+card.img);
  objective.card_type = card.type;
  objective.card_id = card.id;
  objective.object().off("click");
  $( "#btnNextPhase" ).button( "option", "disabled", false );
};

function SetPhase(phase, gamer, gamer_name) {
  $( "#btnNextPhase" ).button( "option", "disabled", true );
  curr_phase = phase;
  curr_gamer = gamer;
  curr_gamer_name = gamer_name
  curr_phase_name = '';
  $( "#btnNextPhase" ).button( "option", "label", "NASTĘPNA FAZA" );
  for(i = 0; i < Hand.length; i++) {
      Hand[i].object().off("click");
  };

  if (gamer_id == curr_gamer) {
    if (phase == 0) {
      curr_phase_name = 'celów';
      if (objective == null) {
        card = objectivePile[objectivePile.length-1];
        card.object().click(function( event ) {
          OnDrawObjective();
        });
      }
      else
      if (!(objective.flipped)) {
        objective.object().click(function( event ) {
          DoFaceUpObjective();
        });
      }
      else {
        $( "#btnNextPhase" ).button( "option", "disabled", false );
      };
    }
    else
    if (phase == 1) {
      curr_phase_name = 'kredchipów';
      if (Hand.length < 7) {
        lblButton2 = 'Dobierz do 7 kart';
      }
      else {
        lblButton2 = '';
      };
      ShowAskMessage('Dodać 4Y czy dobrać karty do 7?', 'Pytanie',
        'Dodaj 4Y', lblButton2,
        function() {
          socket.doSend({'cmd': 'get_nuyens'});
          $( "#btnNextPhase" ).button( "option", "disabled", false );
        },
        function() {
          socket.doSend({'cmd': 'draw_to7'});
          $( "#btnNextPhase" ).button( "option", "disabled", false );
        },
        true
        );
    }
    else
    if (phase == 2) {
      curr_phase_name = 'odświeżania';
      $( "#btnNextPhase" ).button( "option", "disabled", false );
    }
    else
    if (phase == 3) {
      curr_phase_name = 'łażenia';
      $( "#btnNextPhase" ).button( "option", "disabled", false );
    }
    else
    if (phase == 4) {
      curr_phase_name = 'shadowrun';
      $( "#btnNextPhase" ).button( "option", "disabled", false );
    }
    else
    if (phase == 5) {
      curr_phase_name = 'końcowa';
      $( "#btnNextPhase" ).button( "option", "label", "ZAKOŃCZ TURĘ" );
      if (Hand.length > 7) {
        ShowMessage('Kart do wyrzucenia: '+ (Hand.length-7) +'.');
        for(i = 0; i < Hand.length; i++) {
          Hand[i].object().click(DoTrashCard);
        }
      }
      else {
        $( "#btnNextPhase" ).button( "option", "disabled", false );
      };
    };
  };


  $("#currentPhase").text("Faza "+ curr_phase_name);
  //$("#currentGamer").text("Gracz: "+ curr_gamer_name);
};

$(document).ready(function() {

  socket.onMessage = function (evt) {
    data = JSON.parse(evt.data)
    if (data.cmd == 'show_msg') {
      ShowMessage(data.msg);
    }
    else
    if (data.cmd == 'reset') {
      draw_pile_length = data.draw_pile;
      objective_pile_length = data.objective_pile;
      $('#dlgMainMenu').dialog( "close" );
      nuyens = 0;
      initCards2(draw_pile_length, objective_pile_length);
    }
    else
    if (data.cmd == 'draw_card') {
      card = drawPile.pop();
      card.move(100+Hand.length*100, 350);
      card.flip('/static/'+data.img);
      card.card_type = data.type;
      card.card_id = data.id;
      card.object().attr("idx", Hand.length);
      Hand.push(card);
    }
    else
    if (data.cmd == 'add_nuyens') {
      AddNuyens(data.count);
    }
    else
    if (data.cmd == 'next_phase') {
      SetPhase(data.phase, data.gamer, data.gamer_name);
    }
    else
    if (data.cmd == 'trash_card') {
      OnTrashHandCard(data.idx);
    }
    else
    if (data.cmd == 'faceup_objective') {
      OnFaceUpObjective(data.player, data.card);
    }
    else
    if (data.cmd == 'trash_to_drawpile') {
      OnTrashToDrawPile();
    }
    else
    if (data.cmd == 'login') {
      OnLogin(data.gamer);
    }
    else
    if (data.cmd == 'draw_objective') {
      //
    }
    else
    if (data.cmd == 'get_nuyens') {
      //
    }
    else
    if (data.cmd == 'draw_to7') {
      //
    }
    else
    {
      ShowMessage(evt.data);
    };
  };

  socket.onClose = function (evt) {
    setConnected(false);
  };

  socket.onOpen = function (evt) {
    setConnected(true);
  };

  $('#btnLogin').button();
  SetLogged(false);

  $("#ulUsersList").menu();
  $("#ulGameMenu").menu();

  $(".loginUser").click(function( event ) {
    IsDisabled = $(this).hasClass("ui-state-disabled");
    userName = $(this).text();
    userId = $(this).attr("id");
    if (!IsDisabled) {
      socket.doSend({'cmd': 'login', 'params': [userId]});
    };
  });

  $('#btnNewGame').button().click(function( event ) {
    socket.doSend({'cmd': 'new_game'});
    $('#dlgMainMenu').dialog( "close" );
  });

  $('#btnNextPhase').button({
    icons: {
      primary: "ui-icon-circle-arrow-e"
    },
    disabled: true
  }).click(function( event ) {
    socket.doSend({'cmd': 'next_phase'});
  });


});