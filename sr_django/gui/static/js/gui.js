function SetOnClick() {
	$('div.card').click(function() {
        CardId = this.id.substring("card".length)
        rnd = Math.floor((Math.random() * 12) + 1);
        img = img_dir+'front'+rnd+'.jpg';
		talia[CardId].flip(img);
	});
}

$(document).ready(function() {
	initCards(3);
    SetOnClick();

	//talia[0].rotate(50);
    //talia[0].move(500, 150);
    //talia[0].MoveAndRotate(500, 150, 45);
    //talia[0].flip();
	
	$('#startGameButton').click(function() {
		initCards(7);
        SetOnClick();
	});
	
	$('#Test1Button').click(function() {
		for(i = 0; i < talia.length; i++) {
			talia[i].MoveAndRotate(0, 20, 0);
		}
	});

	$('#Test2Button').click(function() {
		for(i = 0; i < talia.length; i++) {
			talia[i].MoveAndRotate(150+i*100, 350, 0);
            talia[i].flip();
		}
	});
	
	$('#Test3Button').click(function() {
		talia[0].MoveAndRotate(120, 100, 90);
		talia[1].MoveAndRotate(240, 100, 0);
		talia[2].MoveAndRotate(590, 150, 0);
        talia[0].flip();
        talia[1].flip();
	});
});