var drawPile = new Array(),
    objectivePile = new Array(),
    Hand = new Array(),
    Trash = new Array(),
    objective = null,

    talia = new Array(),
    img_dir = "/static/images/";

function Card(id) {
	this.id = id;
    this.img = img_dir+"back.jpg";
    this.flipped = false;

	this.rotate = function(angle) {
        $("#" + this.id + " .back img").rotate({duration:500, animateTo:angle});
        $("#" + this.id + " .front img").rotate({duration:500, animateTo:-angle});
	};

	this.move = function(x, y) {
		$("#" + this.id).animate({left:x+"px", top:y+"px"});
	};

	this.MoveAndRotate = function(x, y, angle) {
		this.move(x, y);
		this.rotate(angle);
	};	
	
	this.flip = function(img) {
        if (!$("#" + this.id).hasClass('flipped')) {
            id = this.id;
            if (img) {
                this.img = img;
            };
            $("#" + this.id + " .back img").attr('src',this.img).load(function(){
                $("#" + id).addClass('flipped');
                //ShowBig();
            });
            this.flipped = true;
        }
        else {
            $("#" + this.id).removeClass('flipped');
            //HideBig();
            this.flipped = false;
        };

	};

	this.EnlargeSize = function(x, y) {
		$("#" + this.id + " .back img").addClass('big');
	};

    this.ReduceSize = function(x, y) {
		$("#" + this.id + " .back img").removeClass('big');
	};

	this.getHTML = function() {
        return '<div id="' + this.id + '" class="card"><figure class="front"><img src="'+img_dir+'back.jpg"></figure><figure class="back"><img src="'+img_dir+'back.jpg"></figure></div>';
	};

    this.object = function() {
       return $("#"+this.id);
    };
};

function createCard(iCounter) {
	var card = new Card("card" + iCounter);
	return card;
};

function initState() {
	while(talia.length > 0) {
		talia.pop();
	}
	$('#board').empty();
};

function initState2() {
	while(drawPile.length > 0) {
		drawPile.pop();
	};
	while(objectivePile.length > 0) {
		objectivePile.pop();
	};
	while(Hand.length > 0) {
		Hand.pop();
	};

    objective = null;

	$('#board').empty();
};

function initCards(Count) {

	var iCounter = 0, 
		card = null,
        board = $('#board');

	initState();
	
	for(iCounter = 0; iCounter < Count; iCounter++) {
		card = createCard(iCounter);
		board.append(card.getHTML());
		talia.push(card);
	}

    //SetOnClick();
};

function initCards2(DrawPileLength, ObjectivePileLength) {
	var iCounter1 = 0,
        iCounter2 = 0,
		card = null,
        board = $('#board');

	initState2();

	for(iCounter1 = 0; iCounter1 < DrawPileLength; iCounter1++) {
		card = createCard(iCounter1);
		board.append(card.getHTML());
		drawPile.push(card);
        card.move(170, 50);
	};
	for(iCounter2 = 0; iCounter2 < ObjectivePileLength; iCounter2++) {
		card = createCard(iCounter1 + iCounter2);
		board.append(card.getHTML());
		objectivePile.push(card);
        card.MoveAndRotate(50, 50, 90);
	};

    //SetOnClick();
};

/*
function SetOnClick() {
	$('div.card').click(function() {
        CardId = this.id.substring("card".length)
		talia[CardId].flip();
	});
}

$(document).ready(function() {
	initCards(3);

	//talia[0].rotate(50);
    //talia[0].move(500, 150);
    //talia[0].MoveAndRotate(500, 150, 45);
    //talia[0].flip();
	
	$('#startGameButton').click(function() {
		initCards(7);
	});
	
	$('#Test1Button').click(function() {
		for(i = 0; i < talia.length; i++) {
			talia[i].MoveAndRotate(0, 20, 0);
		}
	});

	$('#Test2Button').click(function() {
		for(i = 0; i < talia.length; i++) {
			talia[i].MoveAndRotate(150+i*100, 350, 0);
            talia[i].flip();
		}
	});
	
	$('#Test3Button').click(function() {
		talia[0].MoveAndRotate(120, 100, 90);
		talia[1].MoveAndRotate(240, 100, 0);
		talia[2].MoveAndRotate(590, 150, 0);
        talia[0].flip();
        talia[1].flip();
	});
});
 */