var
  maxCards = 70;
  cele = [];

function CardMenu(columns, cards) {
    this.firstPos = 0;
    this.columns = columns;
    this.cards = cards;

    this.init = function() {
        for(i = 0; i < this.cards.length; i++) {
            a = 90;
            y = 50;
            if (i < this.columns) {
                x = 50+i*150;
            }
            else {
                x = 50+(this.columns-1)*150;
            }
            this.cards[i].MoveAndRotate(x, y, a);
            this.cards[i].flip();
        };
        this.firstPos = 0;
    };

    this.goLeft = function() {
        if (this.firstPos >= this.cards.length - this.columns) {
            return;
        };
        for(i = 1; i < this.columns ; i++) {
            card = cards[this.firstPos+i];
            a = 90;
            y = 50;
            x = 50 + (i-1)*150;
            card.move(x,y);
        };
        this.firstPos = this.firstPos + 1;
    };

    this.goRight = function() {
        if (this.firstPos <= 0) {
            return;
        };
        for(i = 0; i < this.columns-1; i++) {
            card = cards[this.firstPos+i];
            a = 90;
            y = 50;
            x = 50 + (i+1)*150;
            card.move(x,y);
        };
        this.firstPos = this.firstPos - 1;
    };

    this.init();
}


function SetCounter(id, pos, max) {
   counter = $('#'+id);
   counter.html(pos+'/'+max);
   $( "#progressbar" ).progressbar( "option", "value", pos );
   $( "#progressbar .progress-label" ).html(pos+'/'+max);
}

function ShowMessage(msg) {
    $( "#dialog-message" ).html('<p>'+msg+'</p>').dialog({
        modal: true,
        width: 400,
        resizable: false,
        buttons: {
            Ok: function() {
                $( this ).dialog( "close" );
            }
        }
    });
}

function RemoveFromCele(CeleId) {
    cele[CeleId].move(cele[CeleId].old_left, cele[CeleId].old_top);
    for(i = cele.length-1; i > CeleId; i--) {
        new_pos = $('#'+cele[i-1].id).position();
        cele[i].move(new_pos.left, new_pos.top);
    }
    cele.splice(CeleId, 1);
    SetCounter('counter', cele.length, maxCards);
}

function AddToCele(CardId) {
    card = talia[CardId];
    pos = $('#'+talia[CardId].id).position();
    card.old_left = pos.left;
    card.old_top = pos.top;
    card.move(50+cele.length*150, 300);
    cele.push(card);
    SetCounter('counter', cele.length, maxCards);
}
function SetOnClick() {
	$('div.card').click(function() {
        CardId = this.id.substring("card".length);
        card = talia[CardId];

        CeleId = cele.indexOf(card);
        if (CeleId == -1) {
            if (cele.length == maxCards) {
                ShowMessage('Możesz wybrać maksymalnie '+maxCards+' celów.');
            }
            else {
                AddToCele(CardId);
            }
        }
        else {
            RemoveFromCele(CeleId);
        }

	}).mouseover(function() {
        CardId = this.id.substring("card".length);
        card = talia[CardId];
        card.EnlargeSize();
        }
    ).mouseout(function() {
        CardId = this.id.substring("card".length);
        card = talia[CardId];
        card.ReduceSize();
        });
}

$(document).ready(function() {
    $.getJSON( "/game/deckall.json", function( data ) {
        var items = [],
            len;
        for(key in data.deck) {
          items.push(key);
        }
        len = items.length;
        initCards(len);

        for(i = 0; i < talia.length; i++) {
            talia[i].img = '/static/'+data.deck[i].img;
        };

        //SrcDeck = new CardMenu(7, talia);
        alert(talia.length);
        col = 7;
        for(j = 0; j < Math.ceil(talia.length/col); j++) {
          for(i = 0; i < col; i++) {
              nr = j*7+i;
              if (nr >= talia.length) {
                break;
              }
              //talia[i].MoveAndRotate(50+i*150, 50, 90);
              card = talia[nr];
              if (['Lo', 'Ob'].indexOf(data.deck[nr].type) >= 0) {
                  card.MoveAndRotate(50+i*150, 50+j*140, 90);
              }
              else
              {
                  card.move(50+i*150, 50+j*140);
              }

              //talia[i].move(50+i*95, 50);
              card.flip('/static/'+data.deck[nr].img);
        	};
        };
        /*
        for(i = 7; i < talia.length; i++) {
            talia[i].MoveAndRotate(50+(i-7)*150, 150, 90);
            talia[i].flip('/static/'+data.deck[i].img);
      	}*/

        SetOnClick();
    });

    $('a').button();

	$('#ClearSelect').click(function() {
        while (cele.length > 0) {
            RemoveFromCele(cele.length-1);
        }
	});

	$('#GoLeft').click(function() {
        SrcDeck.goRight();
	});

	$('#GoRight').click(function() {
        SrcDeck.goLeft();
	});

    $( "#progressbar" ).progressbar({
        value: 0,
        max: maxCards
    });

    $( ".progress-label" ).html('0/'+maxCards);
});