var
  DeckMax = 70,
  ObjectivesMax = 6,
  maxCards = 70,
  cardsCount = 0,
  ObjectivesCardsCount = 0,
  deck = {};


function setCounterNr(nr) {
  if (InObjectives()) {
    cardsCount = cardsCount + nr;
    SetCounter('counter', cardsCount, maxCards);
  }
  else
  {
    ObjectivesCardsCount = ObjectivesCardsCount + nr;
    SetCounter('counter', ObjectivesCardsCount, maxCards);
  };
};

function DodajKarte(btn) {
  var
    element = $(btn).parent().parent(),
    counter = element.find(".card_counter"),
    id = counter.attr('id'),
    count = Number(counter.html()),
    max = element.attr('max_cards');

  if (count < max) {
    count = count + 1;
    setCounterNr(1);
    if (id in deck) {
      deck[id] = deck[id] + 1;
    }
    else
    {
      deck[id] = 1;
    };
  }
  else {
    ShowMessage('Można wybrać maksymalnie kart: '+max+'.');
  };
  counter.html(deck[id]);
};

function UsunKarte(btn) {
  var
    element = $(btn).parent().parent(),
    counter = element.find(".card_counter"),
    id = counter.attr('id'),
    count = Number(counter.html());

  if (count > 0) {
    count = count -1;
    setCounterNr(-1);
    if (deck[id] > 1) {
      deck[id] = deck[id] - 1;
    }
    else
    {
      delete deck[id];
    };
  };
  //alert(JSON.stringify(deck, null, 2));
  counter.html(count);
};

function refreshSelected() {
  for (id in deck) {
    var element = $('#'+id);
    if (element) {
      element.html(deck[id]);
    };
  };
};

function SetCounter(id, pos, max) {
  var counter = $('#'+id);
  counter.html(pos+'/'+max);
  $( "#progressbar" ).progressbar( "option", "max", max );
  $( "#progressbar" ).progressbar( "option", "value", pos );
  $( "#progressbar .progress-label" ).html(pos+'/'+max);
}

function ShowMessage(msg) {
    $( "#dialog-message" ).html('<p>'+msg+'</p>').dialog({
        modal: true,
        width: 400,
        resizable: false,
        buttons: {
            Ok: function() {
                $( this ).dialog( "close" );
            }
        }
    });
}

function getTabIndex() {
    return $("#tabs").tabs('option', 'active');
}

function InObjectives() {
  return (getTabIndex() == 6);
}

$(document).ready( function() {

  $( "#progressbar" ).progressbar({
      value: cardsCount,
      max: 6
  });

  $('#save_form').submit(function() {
      data = JSON.stringify(deck, null, 2);
      $("#id_deck").attr("value", data);
      return true;
  });

  $('#save').button();/*.click(function( event ) {
    //alert(JSON.stringify(deck, null, 2));

    $.ajax({
      type: "POST",
      dataType: "json",
      contentType: "application/json",
      url: "/game/create_deck",
      data: {
        deck: JSON.stringify(deck, null, 2),
        csrfmiddlewaretoken: '{{ csrf_token }}'
      },
      dataType: "json"
    });
  });*/

  $.widget( "ui.tabs", $.ui.tabs, {
      options: {
        keyboard: true
      },
      _tabKeydown: function(e) {
        if(this.options.keyboard) {
          this._super( '_tabKeydown' );
        } else {
          return false;
        }
      }
  });

  $( "#tabs" ).tabs({ beforeLoad: function( event, ui ) {
      ui.jqXHR.error(function() {
        ui.panel.html(
        "wczytywanie..." );
      });
    }
  }).addClass( "ui-tabs-vertical ui-helper-clearfix" );
  $( "#tabs li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );
});