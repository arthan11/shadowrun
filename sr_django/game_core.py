# -*- coding: utf-8 -*-
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sr_django.settings")
from srkg.models import *
from django.db.models import Q
from random import shuffle

# six turn phases
phObjective = 0
phCredstick = 1
phRefresh = 2
phLegwork = 3
phShadowrun = 4
phEnd = 5

class Card():
    def __init__(self, db_card, parent=None):
        self.db_card = db_card
        self.parent = parent
        self.turned = False
        self.facedup = False
        self.name = db_card.name
        self.id = db_card.id
        self.type = db_card.__class__.__name__
        self.type2 = self.type[:2].lower()
        self.img = db_card.img.name

    def Deploy(self):
        pass

    def Turn(self):
        self.turned = True

    def Trash(self):
        self.parent.remove(self)
        self.parent.gamer.trash.push(self)

    def Frag(self):
        self.parent.remove(self)

    def FaceUp(self):
        pass

class Pile():
    def __init__(self, gamer):
        self.cards = []
        self.gamer = gamer

    def pop(self):
        return self.cards.pop()

    def push(self, card):
        self.cards.append(card)
        card.parent = self

    def clear(self):
        self.cards = []

    def shuffle(self):
        shuffle(self.cards)

    def count(self):
        return len(self.cards)

    def remove(self, card):
        self.cards.remove(card)

class Gamer():
    def __init__(self, game, id):
        try:
            user = User.objects.get(id=id)
            self.id = user.id
            self.game = game
            self.name = user.name
            self.reputation = 0
            self.kredstick = 0
            self.objective = None
            self.drawPile = Pile(self)
            self.hand = Pile(self)
            self.trash = Pile(self)
            self.objectivePile = Pile(self)
            self.safeHouse = Pile(self)
        except:
            self.name = None


    def OnCmd(self, cmd):
        print cmd

    def InitGame(self ):
        self.reputation = 0
        self.kredstick = 0
        self.objective = None
        self.drawPile.clear()
        self.hand.clear()
        self.trash.clear()
        self.objectivePile.clear()
        self.safeHouse.clear()
        self.GetDeck()
        self.OnCmd({'cmd':'reset', 'draw_pile': self.drawPile.count(), 'objective_pile': self.objectivePile.count()})
        self.AddNuyens(4)
        self.DrawCards(7)

    def AddNuyens(self, count):
        self.kredstick = self.kredstick + count
        self.OnCmd({'cmd':'add_nuyens', 'count':4})

    def AddReputation(self, count):
        self.reputation = self.reputation + count

    def MoveTrashToDrawPile(self):
        self.drawPile.cards = self.trash.cards
        self.trash.clear()
        self.trash.shuffle()
        self.OnCmd({'cmd':'trash_to_drawpile'})

    def DrawCards(self, count):
        for i in range(count):
            card_idx = self.drawPile.count() - 1
            if card_idx < 0:
                self.MoveTrashToDrawPile()
                card_idx = self.drawPile.count() - 1
            card = self.drawPile.pop()
            self.hand.push(card)
            self.OnCmd({'cmd':'draw_card', 'id':card.id, 'img': card.img, 'type': card.type2, 'idx': card_idx})

    def GetDeck(self):
        # objectives
        db_deck = Deck.objects.filter(user__name=self.name, table='ob')
        #print len(db_deck)
        for deck_card in db_deck:
            card = Objective.objects.get(id=deck_card.card_id)
            self.objectivePile.push(Card(card))
        self.objectivePile.shuffle()

        # other cards
        db_deck = Deck.objects.filter(Q(user__name=self.name) & ~Q(table='ob'))
        #print len(db_deck)
        for deck_card in db_deck:
            if deck_card.table == 'ru':
                card = Runner.objects.get(id=deck_card.card_id)
            elif  deck_card.table == 'ch':
                card = Challenge.objects.get(id=deck_card.card_id)
            elif  deck_card.table == 'co':
                card = Contact.objects.get(id=deck_card.card_id)
            elif  deck_card.table == 'ge':
                card = Gear.objects.get(id=deck_card.card_id)
            elif  deck_card.table == 'lo':
                card = Location.objects.get(id=deck_card.card_id)
            elif  deck_card.table == 'sp':
                card = Special.objects.get(id=deck_card.card_id)
            self.drawPile.push(Card(card))
        self.drawPile.shuffle()


    def DrawObjective(self):
        if self.objective == None:
            self.objective = self.objectivePile.pop()
            return True
        else:
            return False

    def FaceUpObjective(self):
        if self.objective <> None:
            if not self.objective.facedup:
                self.objective.facedup = True
                return True
        return False

    def DrawTo7(self):
        if self.hand.count() < 7:
            self.DrawCards(7 - self.hand.count())
            return True
        return False

    '''
    def ObjectivePhase(self):
        if self.objective == None:
            self.objective = self.objectivePile.pop()
        else:
            self.objective.FaceUp()
        #self.game.NextPhase()
        pass

    def CredstickPhase(self):
        #if self.hand.count == 7:
        self.AddNuyens(4)
        #else:

        #self.game.NextPhase()
        pass

    def RefreshPhase(self):
        for card in self.hand.cards:
            card.turned = False
        self.DrawCards(1)
        #self.game.NextPhase()
        pass

    def LegworkPhase(self):
        #self.game.NextPhase()
        pass

    def ShadowrunPhase(self):
        #self.game.NextPhase()
        pass

    def EndPhase(self):
        #self.game.NextPhase()
        pass
    '''
class Game():
    def __init__(self):
        self.gamers = []
        self.currGamer = 0
        self.phase = -1

    def OnCmd(self, cmd):
        print cmd

    def AddGamer(self, id):
        gamer = Gamer(self, id)
        if gamer.name:
            self.gamers.append(gamer)
            #msg = 'Dołączył gracz: %s'.decode('utf-8') % name
            #self.OnCmd({'cmd':'show_msg', 'msg': msg})
            return gamer
        else:
            return None

    def RemoveGamer(self, gamer):
        msg = 'Odszedł gracz: %s'.decode('utf-8') % gamer.name
        self.gamers.remove(gamer)
        self.OnCmd({'cmd':'show_msg', 'msg': msg})

    def StartGame(self):
       self.phase = -1
       for gamer in self.gamers:
           gamer.InitGame()
       self.NextPhase()

    def NextPhase(self):
        self.phase = self.phase + 1
        if self.phase > phEnd: # six phases
            self.phase = 0
            self.currGamer = self.currGamer + 1
            if self.currGamer > len(self.gamers) -1:
                self.currGamer = 0
        '''
        if self.phase == phObjective:
            self.gamers[self.currGamer].ObjectivePhase()
        elif self.phase == phCredstick:
            self.gamers[self.currGamer].CredstickPhase()
        elif self.phase == phRefresh:
            self.gamers[self.currGamer].RefreshPhase()
        elif self.phase == phLegwork:
            self.gamers[self.currGamer].LegworkPhase()
        elif self.phase == phShadowrun:
            self.gamers[self.currGamer].ShadowrunPhase()
        elif self.phase == phEnd:
            self.gamers[self.currGamer].EndPhase()
        '''
        self.OnCmd({'cmd':'next_phase', 'gamer':self.gamers[self.currGamer].id, 'phase':self.phase, 'gamer_name': self.gamers[self.currGamer].name})

if __name__ == "__main__":
    game = Game()
    game.AddGamer('Krzysiek')
    #game.AddGamer('Krzysiek')

    def PrintStatus():
        print 'gracz:', game.currGamer, game.gamers[game.currGamer].name, ', faza:', game.phase
        for card in game.gamers[game.currGamer].hand.cards:
            print '[%s]' % card.type, card.name

        print game.gamers[game.currGamer].objectivePile.count()
        print game.gamers[game.currGamer].objective.name

    game.StartGame()

    for i, gamer in enumerate(game.gamers):
        print gamer.name

    PrintStatus()
    #for i in range(20):
    #  game.NextPhase()
    #  PrintStatus()