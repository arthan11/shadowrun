from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'sr_django.views.home', name='home'),
    # url(r'^sr_django/', include('sr_django.foo.urls')),


    # url(r'^$', 'gpw_analizer.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^', include('gui.urls')),
    url(r'^game/', include('game.urls')),
    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
