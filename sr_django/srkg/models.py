from django.db import models

# abstract classes
class ThreatRating(models.Model):
    attack = models.IntegerField(default=0)
    body = models.IntegerField(default=0)
    armor = models.IntegerField(default=0)
    class Meta:
        abstract = True

class DeployCost(models.Model):
    deploy_cost = models.IntegerField(default=0)
    unkeep_cost = models.IntegerField(default=0)
    class Meta:
        abstract = True

class Named(models.Model):
    name = models.CharField(max_length=100)
    class Meta:
        abstract = True
        ordering = ('name',)
    def __unicode__(self):  # Python 3: def __str__(self):
        return self.name

class NamedWithImg(Named):
    img = models.ImageField(upload_to='img')
    class Meta:
        abstract = True

class CardBase(NamedWithImg):
    description = models.CharField(max_length=200)
    class Meta:
        abstract = True

# some other classes than cards
class SpecialTraits(Named):
    pass

class Race(Named):
    pass

class Proffession(Named):
    pass

class GearType(Named):
    pass

class Skill(NamedWithImg):
    pass

class RunnersSkills(models.Model):
    runner = models.ForeignKey('Runner')
    skill = models.ForeignKey('Skill')
    level = models.IntegerField(default=1)
    def __unicode__(self):  # Python 3: def __str__(self):
        return "%s - %s %d" % (self.runner.name, self.skill.name, self.level)


class RunnersSpecialTraits(models.Model):
    runner = models.ForeignKey('Runner')
    special_trait = models.ForeignKey('SpecialTraits')
    level = models.IntegerField(default=1)
    def __unicode__(self):
        return "%s - %s %d" % (self.runner.name, self.special_trait.name, self.level)

class ChallengesSkills(models.Model):
    group = models.ForeignKey('ChallengesSkillsGroup', related_name='skills')
    skill = models.ForeignKey('Skill')
    level = models.IntegerField(default=1)
    def __unicode__(self):
        return "%s %d" % (self.skill.name, self.level)

class ChallengesSkillsGroup(models.Model):
    def __unicode__(self):
        skills = self.skills.all()
        s = ''
        for skill in skills:
            if (len(s) > 0):
                s = s + ', '
            s = s + '%s %d' % (skill.skill.name, skill.level)
        return s

class ChallengesKeywords(Named):
    pass

# seven types of cards    
class Runner(CardBase, ThreatRating, DeployCost):
    proffession = models.ForeignKey(Proffession)
    race = models.ForeignKey(Race)
    prime_runner = models.BooleanField(default=False)
    def IsUnique(self):
        return (len(self.runnersspecialtraits_set.filter(special_trait__name='unikalna')) > 0)

class Challenge(CardBase, ThreatRating):
    keywords = models.ManyToManyField(ChallengesKeywords)
    challenges_groups = models.ManyToManyField('ChallengesSkillsGroup', null=True, blank=True)

class Gear(CardBase, DeployCost):
    gear_type = models.ForeignKey(GearType)

class Location(CardBase, DeployCost):
    pass

class Contact(CardBase, DeployCost):
    race = models.ForeignKey(Race)

class Objective(CardBase):
    reputation_points = models.IntegerField(default=0)

class Special(CardBase, DeployCost):
    stinger = models.BooleanField()

class User(Named):
    pass

class Deck(models.Model):
    user = models.ForeignKey(User)
    table = models.CharField(max_length=2)
    card_id = models.IntegerField()
    def __unicode__(self):
        return "%s - %s %d" % (self.user, self.table, self.card_id)
