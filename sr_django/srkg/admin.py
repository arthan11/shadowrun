from django.contrib import admin
from srkg.models import (SpecialTraits, Race, Proffession, GearType, Skill,
  RunnersSkills, RunnersSpecialTraits, ChallengesSkills, ChallengesKeywords,
  Runner, Challenge, Gear, Location, Contact, Objective, Special, ChallengesSkillsGroup,
  User, Deck)

class NameSortedAdmin(admin.ModelAdmin):
    ordering = ('name',)

class RunnersSkillsInline(admin.StackedInline):
    model = RunnersSkills
    extra = 0

class ChallengesSkillsInline(admin.StackedInline):
    model = ChallengesSkills
    extra = 0

class RunnersSpecialTraitsInline(admin.StackedInline):
    model = RunnersSpecialTraits
    extra = 0

class ChallengesSkillsGroupAdmin(admin.ModelAdmin):
    inlines = [ChallengesSkillsInline]

class RunnerAdmin(admin.ModelAdmin):
    inlines = [RunnersSkillsInline, RunnersSpecialTraitsInline]
    list_filter = ('proffession','race', 'prime_runner')
    ordering = ('name',)

class GearAdmin(NameSortedAdmin):
    list_filter = ('gear_type',)

class SpecialAdmin(NameSortedAdmin):
    list_filter = ('stinger',)

class RunnersSpecialTraitsAdmin(admin.ModelAdmin):
    list_filter = ('special_trait',)


admin.site.register(User)
admin.site.register(Deck)
admin.site.register(SpecialTraits) 
admin.site.register(Race) 
admin.site.register(Proffession) 
admin.site.register(GearType) 
admin.site.register(Skill, NameSortedAdmin)
admin.site.register(RunnersSkills) 
admin.site.register(RunnersSpecialTraits, RunnersSpecialTraitsAdmin)
admin.site.register(ChallengesSkills)
admin.site.register(ChallengesSkillsGroup, ChallengesSkillsGroupAdmin)
admin.site.register(ChallengesKeywords) 
admin.site.register(Runner, RunnerAdmin)
admin.site.register(Challenge, NameSortedAdmin)
admin.site.register(Gear, GearAdmin)
admin.site.register(Location, NameSortedAdmin)
admin.site.register(Contact, NameSortedAdmin)
admin.site.register(Objective, NameSortedAdmin)
admin.site.register(Special, SpecialAdmin)