# -*- coding: utf-8 -*-
import sys
import tornado.ioloop
import tornado.web
import tornado.websocket as websocket
from game_core import Game
import json

cmds_after_connect = ['login']
cmds_after_login = ['new_game', 'logout']
cmds_not_my_turn = ['use_card', 'logout']
cmds_next_phase = ['next_phase']
cmds_objective_phase = ['draw_objective', 'faceup_objective']
cmds_kredstick_phase = ['draw_to7', 'get_nuyens']
cmds_refresh_phase = ['heal_card', 'next_phase']
cmds_legwork_phase = ['deploy', 'exchange_gears', 'use_card', 'next_phase']
cmds_shadowrun_phase = ['start_shadowrun', 'next_phase']
cmds_end_phase = ['trash_card']


connections = []


def GlobalMsg(msg):
    json_cmd = json.dumps(msg)
    print 'global msg: "%s"' % json_cmd.decode('utf-8')
    #print 'global msg: "%s"' % msg['cmd'].decode('utf-8')
    for con in connections:
        try:
            con.write_message(json_cmd)
        except:
            pass

class MainHandler(websocket.WebSocketHandler):
    def __init__(self, application, request):
        self.gamer = None
        self.cmds = cmds_after_connect
        return super(MainHandler, self).__init__(application, request)


    def Msg(self, msg):
        json_cmd = json.dumps(msg)
        print 'msg to %d: "%s"' % (self.connId(), json_cmd.decode('utf-8'))
        #print 'msg to %d: "%s"' % (self.connId(), msg['cmd'].decode('utf-8'))
        self.write_message(json_cmd)

    def OnClientCmd(self, cmd):
        #print cmd, self.connId()
        #    if cmd['cmd'] == 'reset':
        self.Msg(cmd)

    def connId(self):
        return connections.index(self)

    def open(self):
        connections.append(self)
        print "%d connected" % self.connId()

    def new_game(self):
        game.StartGame()
        #msg = 'Gra rozpoczęta!'
        #GlobalMsg({'cmd': 'show_msg', 'msg': msg})
        for conn in connections:
            if conn.gamer:
                if conn == self:
                    conn.cmds = cmds_objective_phase
                else:
                    conn.cmds = cmds_not_my_turn

    def login(self, id):
        self.gamer = game.AddGamer(id)
        if self.gamer:
            self.gamer.connection = self
            self.gamer.OnCmd = self.OnClientCmd
            self.cmds = cmds_after_login
            self.Msg({'cmd': 'login', 'gamer': self.gamer.id})
        else:
            self.Msg({'cmd': 'show_msg', 'msg': 'Nie udane logowanie!'})

    def logout(self):
        if self.gamer:
            game.RemoveGamer(self.gamer)
            self.gamer = None
            self.cmds = cmds_after_connect

    def trash_card(self, idx):
        if self.gamer:
            try:
                self.gamer.hand.cards[int(idx)].Trash()
                self.Msg({'cmd': 'trash_card', 'idx': idx})
                print self.gamer.hand.count()
                if (self.gamer.hand.count() <= 7):
                    self.cmds = cmds_end_phase + cmds_next_phase
            except:
                pass

    def draw_objective(self):
        if self.gamer:
            if self.gamer.DrawObjective():
                self.cmds = cmds_next_phase
                GlobalMsg({'cmd': 'draw_objective', 'player': self.gamer.name})
            else:
                self.Msg({'cmd': 'show_msg', 'msg': 'Karta celu jest już wyłożona!'})

    def faceup_objective(self):
        if self.gamer:
            if self.gamer.FaceUpObjective():
                card = self.gamer.objective
                card = {'type': card.type2, 'img': card.img, 'id': card.id}
                self.cmds = cmds_next_phase
                GlobalMsg({'cmd': 'faceup_objective', 'player': self.gamer.name, 'card': card})
            else:
                self.Msg({'cmd': 'show_msg', 'msg': 'Nie możesz odwrócić karty celu!'})

    def draw_to7(self):
        if self.gamer:
            if self.gamer.DrawTo7():
                self.cmds = cmds_next_phase
                GlobalMsg({'cmd': 'draw_to7', 'player': self.gamer.name})

    def get_nuyens(self):
        if self.gamer:
            self.gamer.AddNuyens(4)
            self.cmds = cmds_next_phase
            GlobalMsg({'cmd': 'get_nuyens', 'player': self.gamer.name, 'count': 4})

    def next_phase(self):
       if self.gamer:
           game.NextPhase()
           if (game.gamers[game.currGamer] <> self.gamer):
               self.gamer.cmds = cmds_not_my_turn
               new_gamer = game.gamers[game.currGamer]
               new_gamer.cmds = cmds_objective_phase
               if new_gamer.objective:
                   if new_gamer.objective.facedup:
                       new_gamer.connection.cmds = new_gamer.connection.cmds + cmds_next_phase
           else:
               if (game.phase == 0):
                   self.cmds = cmds_objective_phase
                   if self.gamer.objective:
                       if self.gamer.objective.facedup:
                           self.cmds = self.cmds + cmds_next_phase
               elif (game.phase == 1):
                   self.cmds = cmds_kredstick_phase
               elif (game.phase == 2):
                   self.cmds = cmds_refresh_phase
                   self.gamer.DrawCards(1)
               elif (game.phase == 3):
                   self.cmds = cmds_legwork_phase
               elif (game.phase == 4):
                   self.cmds = cmds_shadowrun_phase
               elif (game.phase == 5):
                   self.cmds = cmds_end_phase
                   if (self.gamer.hand.count() <= 7):
                       self.cmds = self.cmds + cmds_next_phase


    def on_message(self, message):
        print self.request.remote_ip
        try:
            msg = json.loads(message)
        except:
            return

        if 'cmd' in msg:
            cmd = msg['cmd']
        else:
            cmd = ''
        if 'params' in msg:
            params = msg['params']
        else:
            params = []
        print cmd

        '''
        for con in connections:
            if con == self:
               con.write_message('ty:' + message)
            else:
               con.write_message('con %d:' % self.connId() +  message)
        '''
        if cmd in self.cmds:
            if len(params) > 0:
                getattr(self, cmd)(*params)
            else:
                getattr(self, cmd)()
        else:
            msg = 'Niedozwolona operacja!\nMożliwe operacje to:'
            for cmd in self.cmds:
                msg = msg + '\n' + cmd
            self.Msg({'cmd':'show_msg', 'msg': msg})

    def on_close(self):
        self.logout()
        conn_id = self.connId()
        connections.remove(self)
        print "%d disconnected" % conn_id

application = tornado.web.Application([
    (r"/", MainHandler),
])

if __name__ == "__main__":
    game = Game()
    game.OnCmd = GlobalMsg
    try:
        application.listen(1337)
        print 'Serwer włączony.'.decode('utf-8')
    except:
        print 'Nie można właczyć serwera. Czyżby port był już w użyciu?'.decode('utf-8')
        sys.exit();
    tornado.ioloop.IOLoop.instance().start()
    tornado.ioloop.IOLoop.instance().stop()
